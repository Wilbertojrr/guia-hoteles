$(function(){
      $("[data-toggle='tooltip']").tooltip();
    });

    $(function () {
    $('[data-toggle="popover"]').popover()
    })

    $('.carousel').carousel({
      interval: 2500  
    }) 

    $( '.btncontacto' ).click(function() {
        $(this).removeClass('btn-primary');
        $(this).addClass('btn-success');         
      });
      
    $('#contacto-modal').on('show.bs.modal', function (e) { 
      console.log('se esta ejecutando')   
      $('#contactoBtn1,#contactoBtn2,#contactoBtn3,#contactoBtn4,#contactoBtn5,#contactoBtn6').prop('disabled',true);       
    })

    $('#contacto-modal').on('shown.bs.modal', function (e) {
      console.log('el modal se termino de activar');
    })

    $('#contacto-modal').on('hide.bs.modal', function (e) {
      console.log('el modal se esta cerrando');
      $('#contactoBtn1,#contactoBtn2,#contactoBtn3,#contactoBtn4,#contactoBtn5,#contactoBtn6').removeClass('btn-success');
      $('#contactoBtn1,#contactoBtn2,#contactoBtn3,#contactoBtn4,#contactoBtn5,#contactoBtn6').addClass('btn-primary');
    })

    $('#contacto-modal').on('hidden.bs.modal', function (e) {
      console.log('el modal termino de cerrarse');
      $('.btn').prop('disabled',false);
    })